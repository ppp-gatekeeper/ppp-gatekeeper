<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@


	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Nigel</firstname>">
  <!ENTITY dhsurname   "<surname>Kukard</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>2015-05-28 08:41</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>8</manvolnum>">
  <!ENTITY dhemail     "<email>nkukard@lbsd.net</email>">
  <!ENTITY dhusername  "nkukard">
  <!ENTITY dhucpackage "<refentrytitle>PPP-GATEKEEPER</refentrytitle>">
  <!ENTITY dhpackage   "ppp-gatekeeper">

  <!ENTITY author      "Nigel Kukard">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL-3</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2010-2015</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>PPP GateKeeper is a daemon that manages redundant and failover PPPOE connections.</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>

      <arg><option>--config <replaceable>config file</replaceable></option></arg>
      <arg><option>--fg</option></arg>
      <arg><option>--help</option></arg>

    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>
    <para>
 PPP Gatekeeper is a daemon that manages PPPOE connections supporting
 various levels of redundancy and failover.
 .
 Traffic can be routed using round-robin and random strategies over links
 of similar priority. Static IP route lists can also be provided for specific
 routing purposes. DNS can also be load balanced over multiple links.
	</para>
  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>These programs follow the usual &gnu; command line syntax,
      with long options starting with two dashes (`-').  A summary of
      options is included below.</para>

    <variablelist>
      <varlistentry>
        <term><option>--config</option></term>
        <listitem>
          <para>Specify config file to use.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--fg</option></term>
        <listitem>
          <para>Run in foreground, don't daemonize.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--help</option></term>
        <listitem>
          <para>Show summary of options.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  <refsect1>
    <title>SEE ALSO</title>

    <para>pppd (8).</para>

  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &author; &lt;&dhemail;&gt;.
      Permission is granted to copy, distribute and/or modify this
      document under the terms of the &gnu; General Public License,
      Version 3 any later version published by the Free Software
      Foundation.
    </para>
    <para>
      On Debian systems, the complete text of the GNU General Public
      License can be found in /usr/share/common-licenses/GPL-3.
    </para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->
